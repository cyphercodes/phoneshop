<?php
Yii::setAlias('@uploads', 'http://phoneshop.local/uploads'); //Don't put trailing slash, put that in DB path column
Yii::setAlias('@uploadsPath', realpath(dirname(__FILE__) . '/../../frontend/web/uploads'));
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
