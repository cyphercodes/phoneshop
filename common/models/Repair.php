<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "repair".
 *
 * @property integer $id
 * @property string $title
 * @property string $product_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Repair extends \yii\db\ActiveRecord {

    const STATUS_INPROGRESS = 10;
    const STATUS_FIXED = 20;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'repair';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'product_id'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_INPROGRESS],
            ['status', 'in', 'range' => [self::STATUS_FIXED, self::STATUS_INPROGRESS]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'product_id' => 'Product ID',
            'status' => 'Status',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function statusString() {
        if ($this->status == self::STATUS_INPROGRESS) {
            return 'In Progress';
        } else {
            return 'Fixed';
        }
    }

    public function statusColor() {
        if ($this->status == self::STATUS_INPROGRESS) {
            return 'red';
        } else {
            return 'green';
        }
    }

    public static function statusOptions() {
        return [self::STATUS_INPROGRESS => 'In Progress', self::STATUS_FIXED => 'Fixed'];
    }
}
