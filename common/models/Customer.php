<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $email
 *
 * @property Sale[] $sales
 */
class Customer extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['firstname'], 'required'],
            [['firstname', 'lastname', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            [['email'], 'string', 'max' => 191],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales() {
        return $this->hasMany(Sale::className(), ['customer_id' => 'id']);
    }

    public function getName() {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getTotalspent() {
        $sum = 0;
        foreach ($this->sales as $sale) {
            $sum += $sale->cost;
        }
        return $sum;
    }

    public static function listAll() {
        $customers = self::find()->all();
        $arr = [];
        foreach ($customers as $customer) {
            $arr[$customer->id] = $customer->name;
        }
        return $arr;
    }

    public function isDiscount() { // Check if customer is eligible for discount
        return ($this->totalspent > 500000);
    }
}
