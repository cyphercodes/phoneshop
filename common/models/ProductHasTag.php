<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_has_tag".
 *
 * @property integer $product_id
 * @property integer $tag_id
 *
 * @property Product $product
 * @property Tag $tag
 */
class ProductHasTag extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_has_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'tag_id'], 'required'],
            [['product_id', 'tag_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'product_id' => 'Product ID',
            'tag_id' => 'Tag ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag() {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }

    public function beforeSave($insert) {
        if($insert){
            if($model = self::find()->where(['product_id'=>$this->product_id, 'tag_id'=>$this->tag_id])){
                return $model;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
