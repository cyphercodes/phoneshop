<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ProductHasTag[] $productHasTags
 */
class Tag extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasTags() {
        return $this->hasMany(ProductHasTag::className(), ['tag_id' => 'id']);
    }

    public function beforeSave($insert) {
        if ($insert) {
            if ($tag = self::find()->where(['title' => $this->title])->one()) {
                return $tag;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public static function allTags() {
        $tags = self::find()->all();
        $arr = [];
        foreach ($tags as $tag) {
            $arr[$tag->title] = $tag->title;
        }
        return $arr;
    }
}
