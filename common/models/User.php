<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $email
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface {

    const TYPE_ADMIN = 0;
    const TYPE_SALES = 10;
    const TYPE_TECHNICAL = 20;

    public $password;


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['auth_key', 'password_hash', 'email'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'email'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['type', 'default', 'value' => self::TYPE_ADMIN],
            ['type', 'in', 'range' => [self::TYPE_ADMIN, self::TYPE_SALES, self::TYPE_TECHNICAL]],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'email' => 'Email',
            'type' => 'Type',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
            'typelabel' => 'Type',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function isAdmin() {
        return $this->type == self::TYPE_ADMIN;
    }

    public function isSales() {
        return $this->type == self::TYPE_SALES;
    }

    public function isTechnical() {
        return $this->type == self::TYPE_TECHNICAL;
    }

    public function getTypelabel() {
        if ($this->type == self::TYPE_ADMIN) {
            return 'Admin';
        } else if ($this->type == self::TYPE_SALES) {
            return 'Sales';
        } else if ($this->type == self::TYPE_TECHNICAL) {
            return 'Technical';
        } else {
            return 'Unknown';
        }
    }

    public function create() {
        $this->setPassword($this->password);
        $this->generateAuthKey();
        return $this->save();
    }

}
