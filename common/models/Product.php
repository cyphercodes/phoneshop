<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property double $price
 * @property integer $available
 * @property integer $sold
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property integer $subcategory_id
 *
 * @property OfferHasProduct[] $offerHasProducts
 * @property Category $category
 * @property Subcategory $subcategory
 * @property ProductHasTag[] $productHasTags
 * @property Sale[] $sales
 */
class Product extends \yii\db\ActiveRecord {
    const STATUS_AVAILABLE = 10;
    const STATUS_SOLDOUT = 20;

    public $imgTmp;

    public $tagsTmp;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'price', 'available', 'category_id', 'subcategory_id'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['available', 'sold', 'created_at', 'updated_at', 'category_id', 'subcategory_id'], 'integer'],
            ['available', 'default', 'value' => 0],
            ['sold', 'default', 'value' => 0],
            [['title', 'image'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subcategory::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'price' => 'Price',
            'available' => 'Available',
            'sold' => 'Sold',
            'image' => 'Image',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
            'category_id' => 'Category',
            'subcategory_id' => 'Subcategory',
            'status' => 'Status',
            'tagsTmp' => 'Tags',
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function allProductsArray() {
        $data = self::find()->all();
        $arr = [];
        foreach ($data as $one) {
            $arr[$one->id] = $one->title;
        }
        return $arr;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferHasProducts() {
        return $this->hasMany(OfferHasProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory() {
        return $this->hasOne(Subcategory::className(), ['id' => 'subcategory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHasTags() {
        return $this->hasMany(ProductHasTag::className(), ['product_id' => 'id']);
    }

    public function getTags() {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('product_has_tag', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales() {
        return $this->hasMany(Sale::className(), ['product_id' => 'id']);
    }

    public function realAvailable() {
        return $this->available - $this->sold;
    }

    public function getStatus() {
        if ($this->available <= $this->sold) {
            return self::STATUS_SOLDOUT; //SOLD OUT
        } else {
            return self::STATUS_AVAILABLE;
        }
    }

    public function src() {
        if ($this->image) {
            return Yii::getAlias('@uploads') . $this->image;
        } else {
            return 'http://placehold.it/600x600';
        }
    }

    public function uploadImg() {
        $imageInfo = getimagesize($this->imgTmp->tempName);
        switch ($imageInfo[2]) {
            case IMAGETYPE_JPEG :
                $src = imagecreatefromjpeg($this->imgTmp->tempName);
                break;
            case IMAGETYPE_PNG :
                $src = imagecreatefrompng($this->imgTmp->tempName);
                break;
            default :
                $this->addError('image', 'Error!');
                return false;
        }
        $uniqueid = uniqid();
        $imagedir = Yii::getAlias('@uploadsPath');
        $directory = $imagedir . DIRECTORY_SEPARATOR . "image_" . $uniqueid . '.jpg';

        $directorydb = '/' . "image_" . $uniqueid . '.jpg';

        if (!file_exists($imagedir)) {
            mkdir($imagedir, 0777, true);
        }
        imagejpeg($src, $directory);
        imagedestroy($src);
        $this->image = $directorydb;
        return $this->save();
    }

    public function beforeValidate() {
        if (!$this->isNewRecord) {
            if ($this->available < $this->sold) {
                $this->addError('available', 'Available can only be more than or equal to sold...');
            }
        }
        return parent::beforeValidate();
    }

    public static function listAvailable() {
        $products = self::find()->where('available > sold')->all();
        $arr = [];
        foreach ($products as $product) {
            $arr[$product->id] = $product->title;
        }
        return $arr;
    }

    public function getTagsarray() {
        $arr = [];
        foreach ($this->tags as $tag) {
            $arr[] = $tag->title;
        }
        return $arr;
    }

    public function afterSave($insert, $changedAttributes) {
        if (!is_array($this->tagsTmp)) {
            $this->tagsTmp = [];
        }
        foreach ($this->productHasTags as $hasTag) {
            if (!in_array($hasTag, $this->tagsTmp)) {
                $hasTag->delete();
            }
        }
        foreach ($this->tagsTmp as $tag) {
            if (!$find = Tag::find()->where(['title' => $tag])->one()) {
                $newTag = new Tag();
                $newTag->title = $tag;
                $newTag->save();
                $find = $newTag;
            }
            $hasTag = new ProductHasTag();
            $hasTag->tag_id = $find->id;
            $hasTag->product_id = $this->id;
            if (!$hasTag->save()) {
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    public function description() {
        if ($this->description) {
            return $this->description;
        }
        return 'This item does not have a description...';
    }

}