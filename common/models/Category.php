<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Product[] $products
 * @property Subcategory[] $subcategories
 */
class Category extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategories() {
        return $this->hasMany(Subcategory::className(), ['category_id' => 'id']);
    }

    public function title() {
        return ucwords($this->title);
    }

    public static function listAll() {
        $categories = self::find()->all();
        $arr = [];
        foreach ($categories as $category) {
            $arr[$category->id] = $category->title;
        }
        return $arr;
    }

    public static function getArray() {
        $categories = self::find()->all();
        $arr = [];
        foreach ($categories as $category) {
            if (count($category->products) > 0) {
                $arr[] = ['id' => $category->id, 'title' => $category->title()];
            }
        }
        return $arr;
    }
}
