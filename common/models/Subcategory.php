<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subcategory".
 *
 * @property integer $id
 * @property string $title
 * @property integer $category_id
 *
 * @property Product[] $products
 * @property Category $category
 */
class Subcategory extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'category_id'], 'required'],
            [['category_id'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['subcategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function title() {
        return ucwords($this->title);
    }

    public static function listFor($category_id) {
        $arr = [];
        $subs = self::find()->where('category_id = :category_id', [':category_id' => $category_id])->all();
        foreach ($subs as $sub) {
            $arr[$sub->id] = $sub->title;
        }
        return $arr;
    }
}
