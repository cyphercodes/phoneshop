<?php

namespace common\components;

class Currency {

    public static function format($value) {
        return number_format($value, 0) . ' L.L';
    }

    public static function dateFormat() {
        return 'd/m/y  g:i A';
    }

}