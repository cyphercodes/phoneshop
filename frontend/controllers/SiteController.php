<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Offer;
use common\models\Product;
use common\models\Subcategory;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $products = Product::find()->limit(6)->all();
        $offers = Offer::find()->limit(15)->all();
        return $this->render('index', [
            'products' => $products,
            'offers'=>$offers,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionCategory($id, $sub = null) {
        if (!$model = Category::findOne($id)) {
            throw new NotFoundHttpException();
        }
        if (!$subcategory = Subcategory::findOne($sub)) {
            $products = Product::find()->where(['category_id' => $id])->all();
        } else {
            $products = Product::find()->where(['subcategory_id' => $sub])->all();
        }
        return $this->render('category', [
            'model' => $model,
            'subcategory' => $subcategory,
            'products' => $products,
        ]);
    }
}