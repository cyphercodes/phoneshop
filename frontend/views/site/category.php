<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = $model->title;
?>
<div class="row margin-top-10">
    <div class="col m2 s12">
        <div class="collection">
            <a href="<?= Url::to(['site/category', 'id' => $model->id]) ?>" class="collection-item <?= $subcategory ? '' : 'active' ?>">All</a>
            <?php foreach ($model->subcategories as $sub) { ?>
                <a href="<?= Url::to(['site/category', 'id' => $model->id, 'sub' => $sub->id]) ?>" class="collection-item <?= ($subcategory && $subcategory->id == $sub->id) ? 'active' : '' ?>"><?= $sub->title() ?></a>
            <?php } ?>
        </div>
    </div>
    <div class="col m10 s12">
        <?php foreach ($products as $product) { ?>
            <?= $this->render('_product', ['product' => $product]); ?>
        <?php } ?>
        <?php if (count($products) < 1) { ?>
            <p class="center">Currently, there is no products under this category...</p>
        <?php } ?>
    </div>
</div>

