<div class="col s12 m4">
    <div class="card small product hoverable">
        <div class="card-image">
            <img class="materialboxed" src="<?= $product->src() ?>"/>
            <span class="card-title"><?= $product->title ?></span>
        </div>
        <div class="card-content">
            <p><?= $product->description() ?></p>
        </div>
        <div class="card-action">
            <a href="#"><?= \common\components\Currency::format($product->price) ?></a>
        </div>
    </div>
</div>
