<?php

/* @var $this yii\web\View */

$this->title = 'Phone Shop';
?>
<div class="row">
    <div class="col s10 offset-s1">
        <h4 class="center">Latest Items</h4>

        <div class="row">
            <?php foreach ($products as $product) { ?>
                <?= $this->render('_product', ['product' => $product]); ?>
            <?php } ?>
        </div>
        <hr/>
        <h4 class="center">Latest Offers</h4>

        <div class="row">
            <?php foreach ($offers as $offer) { ?>
                <?= $this->render('_offer', ['offer' => $offer]); ?>
            <?php } ?>
        </div>
    </div>
</div>
