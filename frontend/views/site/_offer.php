<div class="col s12 m4">
    <div class="card small product hoverable">
        <div class="card-image">
            <img class="materialboxed" src="<?= $offer->src ?>"/>
            <span class="card-title"><?= $offer->title ?></span>
        </div>
        <div class="card-content">
            <p><?= Yii::$app->formatter->asPercent($offer->discount / 100); ?> discount</p>
            <p>Includes:
                <?php foreach ($offer->products as $product) { ?>
                    <br><?= $product->title; ?>
                <?php } ?>
            </p>
        </div>
        <div class="card-action">
            <a class="red-text" href="#"><?= \common\components\Currency::format($offer->price) ?></a>
        </div>
    </div>
</div>
