<footer class="page-footer blue">
    <div class="footer-copyright blue">
        <div class="container">
            © <?= date('Y') ?> PhoneShop
            <span class="grey-text text-lighten-4 right">Created by Ahmad Ginzarly</span>
        </div>
    </div>
</footer>