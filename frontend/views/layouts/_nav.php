<?php
use yii\helpers\Url;

$categories = \common\models\Category::getArray();
?>

<nav class="blue">
    <div class="nav-wrapper container">
        <a href=" <?= Url::toRoute(['/site/index']); ?>" class="brand-logo">PhoneShop</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
            <?php foreach ($categories as $category) { ?>
                <li><a href="<?= Url::to(['site/category', 'id' => $category['id']]) ?>"><?= $category['title']; ?></a>
                </li>
            <?php } ?>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
            <?php foreach ($categories as $category) { ?>
                <li>
                    <a href="<?= Url::to(['site/category', 'id' => $category['id']]) ?>"><?= $category['title']; ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</nav>