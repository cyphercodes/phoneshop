<?php

use yii\db\Migration;
use yii\db\Schema;

class m161104_181022_product_has_tag_primarykey extends Migration {
    public function safeUp() {
        $this->addColumn('{{%product_has_tag}}', 'id', Schema::TYPE_PK);
        $this->addColumn('{{%offer_has_product}}', 'id', Schema::TYPE_PK);
        $this->createIndex('id_UNIQUE', '{{%product_has_tag}}', 'id', 1);
        $this->createIndex('id_UNIQUE', '{{%offer_has_product}}', 'id', 1);
    }

    public function safeDown() {
        $this->dropColumn('{{%product_has_tag}}', 'id');
        $this->dropColumn('{{%offer_has_product}}', 'id');
    }
}
