<?php

use yii\db\Migration;

class m160519_163446_initialdata extends Migration {

    public function up() {
        $this->insert('user', ['auth_key' => 'cFZRRa5rOLtGfvyHl63kgz3sar-5iNiP', 'password_hash' => '$2y$13$MgycOkUj7frLCwgTUY9od.nEgAryMMe1j9WE.yFYTsaevNNZKSkOy', 'email' => 'admin@phoneshop.local', 'type' => 0, 'updated_at' => time(), 'created_at' => time()]);
        $this->insert('user', ['auth_key' => 'tw3qG2AwZu9kiU-Djhxr_wd1vGsSHInY', 'password_hash' => '$2y$13$/TPuLCPW1Na7kWsvK1J75uqsqr9K8CkkprrufjF8ajhJFTyrLqWrS', 'email' => 'sales@phoneshop.local', 'type' => 10, 'updated_at' => time(), 'created_at' => time()]);
        $this->insert('user', ['auth_key' => 'RneGEiD0Re5erKTZgC0yWVl2vffBc3xo', 'password_hash' => '$2y$13$DfX4.ZwzSR0LiBI0QyAbwufxMon0fgdvfhCL4uJ/lBiCcaT7lEZAq', 'email' => 'technical@phoneshop.local', 'type' => 20, 'updated_at' => time(), 'created_at' => time()]);
    }

    public function down() {
        $this->delete('user');
        return false;
    }

}
