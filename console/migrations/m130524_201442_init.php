<?php

use yii\db\Migration;
use yii\db\Schema;

class m130524_201442_init extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product}}', [
            'id' => Schema::TYPE_PK . '',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'description' => Schema::TYPE_TEXT . '',
            'price' => Schema::TYPE_FLOAT . ' NOT NULL',
            'available' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'sold' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'image' => Schema::TYPE_STRING . '(255)',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'subcategory_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%product}}', 'id', 1);
        $this->createIndex('fk_product_category1_idx', '{{%product}}', 'category_id', 0);
        $this->createIndex('fk_product_subcategory1_idx', '{{%product}}', 'subcategory_id', 0);
        $this->createTable('{{%category}}', [
            'id' => Schema::TYPE_PK . '',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%category}}', 'id', 1);
        $this->createTable('{{%subcategory}}', [
            'id' => Schema::TYPE_PK . '',
            'title' => Schema::TYPE_STRING . '(45) NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%subcategory}}', 'id', 1);
        $this->createIndex('fk_subcategory_category1_idx', '{{%subcategory}}', 'category_id', 0);
        $this->createTable('{{%customer}}', [
            'id' => Schema::TYPE_PK . '',
            'firstname' => Schema::TYPE_STRING . '(255)',
            'lastname' => Schema::TYPE_STRING . '(255)',
            'phone' => Schema::TYPE_STRING . '(255)',
            'email' => Schema::TYPE_STRING . '(191)',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%customer}}', 'id', 1);
        $this->createTable('{{%offer_has_product}}', [
            'offer_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'product_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('fk_offer_has_product_product1_idx', '{{%offer_has_product}}', 'product_id', 0);
        $this->createIndex('fk_offer_has_product_offer1_idx', '{{%offer_has_product}}', 'offer_id', 0);
        $this->createTable('{{%product_has_tag}}', [
            'product_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'tag_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('fk_product_has_tag_tag1_idx', '{{%product_has_tag}}', 'tag_id', 0);
        $this->createIndex('fk_product_has_tag_product1_idx', '{{%product_has_tag}}', 'product_id', 0);
        $this->createTable('{{%tag}}', [
            'id' => Schema::TYPE_PK . '',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%tag}}', 'id', 1);
        $this->createTable('{{%sale}}', [
            'id' => Schema::TYPE_PK . '',
            'quantity' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'cost' => Schema::TYPE_FLOAT . ' NOT NULL',
            'note' => Schema::TYPE_TEXT . '',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'product_id' => Schema::TYPE_INTEGER . '(11)',
            'customer_id' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%sale}}', 'id', 1);
        $this->createIndex('fk_sale_product_idx', '{{%sale}}', 'product_id', 0);
        $this->createIndex('fk_sale_customer1_idx', '{{%sale}}', 'customer_id', 0);
        $this->createTable('{{%repair}}', [
            'id' => Schema::TYPE_PK . '',
            'title' => Schema::TYPE_STRING . '(255)',
            'product_id' => Schema::TYPE_STRING . '(255)',
            'status' => Schema::TYPE_SMALLINT . '(6) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%repair}}', 'id', 1);
        $this->createTable('{{%offer}}', [
            'id' => Schema::TYPE_PK . '',
            'status' => Schema::TYPE_SMALLINT . '(6) NOT NULL',
            'price' => Schema::TYPE_FLOAT . ' NOT NULL',
            'discount' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('id_UNIQUE', '{{%offer}}', 'id', 1);
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK . '',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . '(255) NOT NULL',
            'email' => Schema::TYPE_STRING . '(191) NOT NULL',
            'type' => Schema::TYPE_SMALLINT . '(6) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ], $tableOptions);

        $this->createIndex('email', '{{%user}}', 'email', 1);
        $this->addForeignKey('fk_product_category_id', '{{%product}}', 'category_id', 'category', 'id');
        $this->addForeignKey('fk_product_subcategory_id', '{{%product}}', 'subcategory_id', 'subcategory', 'id');
        $this->addForeignKey('fk_subcategory_category_id', '{{%subcategory}}', 'category_id', 'category', 'id');
        $this->addForeignKey('fk_offer_has_product_offer_id', '{{%offer_has_product}}', 'offer_id', 'offer', 'id');
        $this->addForeignKey('fk_offer_has_product_product_id', '{{%offer_has_product}}', 'product_id', 'product', 'id');
        $this->addForeignKey('fk_product_has_tag_product_id', '{{%product_has_tag}}', 'product_id', 'product', 'id');
        $this->addForeignKey('fk_product_has_tag_tag_id', '{{%product_has_tag}}', 'tag_id', 'tag', 'id');
        $this->addForeignKey('fk_sale_product_id', '{{%sale}}', 'product_id', 'product', 'id');
        $this->addForeignKey('fk_sale_customer_id', '{{%sale}}', 'customer_id', 'customer', 'id');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_product_category_id', '{{%product}}');
        $this->dropForeignKey('fk_product_subcategory_id', '{{%product}}');
        $this->dropForeignKey('fk_subcategory_category_id', '{{%subcategory}}');
        $this->dropForeignKey('fk_offer_has_product_offer_id', '{{%offer_has_product}}');
        $this->dropForeignKey('fk_offer_has_product_product_id', '{{%offer_has_product}}');
        $this->dropForeignKey('fk_product_has_tag_product_id', '{{%product_has_tag}}');
        $this->dropForeignKey('fk_product_has_tag_tag_id', '{{%product_has_tag}}');
        $this->dropForeignKey('fk_sale_product_id', '{{%sale}}');
        $this->dropForeignKey('fk_sale_customer_id', '{{%sale}}');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%category}}');
        $this->dropTable('{{%subcategory}}');
        $this->dropTable('{{%customer}}');
        $this->dropTable('{{%offer_has_product}}');
        $this->dropTable('{{%product_has_tag}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%sale}}');
        $this->dropTable('{{%repair}}');
        $this->dropTable('{{%offer}}');
        $this->dropTable('{{%user}}');
    }
}
