Phone Shop System Software
===============================

Project by:

Ahmad Ginzarly



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.

CONFIGURATION
-------------

### Database

Configure database in `config/db.php`, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=phoneshop',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Apply database migrations:
```
php yii migrate
```

### Configure Environment

Add a virtual host to apache:

```
<VirtualHost *:80>
    ServerName phoneshop.local
    # use correct directory
    DocumentRoot "c:/wamp/www/phoneshop/frontend/web"

    # use correct directory
    <Directory "c:/wamp/www/phoneshop/frontend/web">
        # use mod_rewrite for pretty URL support
		RewriteEngine on
		# If a directory or a file exists, use the request directly
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		# Otherwise forward the request to index.php
		RewriteRule . index.php
	</Directory>
</VirtualHost>
```

```
<VirtualHost *:80>
    ServerName backend.phoneshop.local
    # use correct directory
    DocumentRoot "c:/wamp/www/phoneshop/backend/web"

    # use correct directory
    <Directory "c:/wamp/www/phoneshop/backend/web">
        # use mod_rewrite for pretty URL support
		RewriteEngine on
		# If a directory or a file exists, use the request directly
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		# Otherwise forward the request to index.php
		RewriteRule . index.php
	</Directory>
</VirtualHost>
```


Add this line to the hosts file
On Windows: C:\Windows\System32\drivers\etc\hosts
```
127.0.0.1   phoneshop.local
127.0.0.1   backend.phoneshop.local
```

### Users

```
Admin:
admin@phoneshop.local
password123
```

```
Sales:
sales@phoneshop.local
password123
```

```
Technical
technical@phoneshop.local
password123
```

### DIRECTORY STRUCTURE


```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```
