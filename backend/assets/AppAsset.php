<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'lib/font-awesome/css/font-awesome.min.css',
        'lib/materialize/css/materialize.min.css',
        'css/style.css',
    ];
    public $js = [
        'lib/materialize/js/materialize.min.js',
        'js/core.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
//        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
