<?php
namespace backend\controllers;

use common\models\Customer;
use common\models\Product;
use common\models\Repair;
use common\models\Sale;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        if (Yii::$app->user->identity->isTechnical()) {
            $this->redirect(['technical/index']);
        }
        if (Yii::$app->user->identity->isSales()) {
            $this->redirect(['sales/index']);
        }

        if (isset(Yii::$app->request->get()['date_range']) && !empty(Yii::$app->request->get()['date_range'])) {
            $fromToDate = explode(" to ", urldecode(Yii::$app->request->get()['date_range']));
            if (count($fromToDate) !== 2) {
                Yii::$app->session->setFlash('error', 'Invalid date input!');
                return $this->redirect(['site/index']);
            }
            $fromDate = strtotime($fromToDate[0]);
            $toDate = strtotime($fromToDate[1]);

            $salesDb = Yii::$app->getDb()->createCommand("SELECT UNIX_TIMESTAMP(DATE(FROM_UNIXTIME(created_at))) as '0', COUNT(id) as '1' from sale WHERE created_at BETWEEN " . $fromDate . " AND " . $toDate . "  GROUP BY DATE(FROM_UNIXTIME(created_at)) ORDER BY DATE(FROM_UNIXTIME(created_at)) ASC")->queryAll();

            $salesAmountDb = Yii::$app->getDb()->createCommand("SELECT UNIX_TIMESTAMP(DATE(FROM_UNIXTIME(created_at))) as '0', SUM(cost) as '1' from sale WHERE created_at BETWEEN " . $fromDate . " AND " . $toDate . "  GROUP BY DATE(FROM_UNIXTIME(created_at)) ORDER BY DATE(FROM_UNIXTIME(created_at)) ASC")->queryAll();

            $salesDisplay = [];
            $salesSum = 0;
            for ($i = 0; $i < count($salesDb); $i++) {
                $salesDisplay[$i][] = intval($salesDb[$i][0]) * 1000;
                $salesDisplay[$i][] = (float)$salesDb[$i][1];
                $salesSum += (float)$salesDb[$i][1];
            }
            $salesAmountDisplay = [];
            $salesAmountSum = 0;
            for ($i = 0; $i < count($salesAmountDb); $i++) {
                $salesAmountDisplay[$i][] = intval($salesAmountDb[$i][0]) * 1000;
                $salesAmountDisplay[$i][] = (float)$salesAmountDb[$i][1];
                $salesAmountSum += (float)$salesAmountDb[$i][1];
            }
        } else {
            $salesDisplay = null;
            $salesAmountDisplay = null;
            $salesSum = 0;
            $salesAmountSum = 0;
        }

        $stats = [];

        // Products
        $products = Product::find()->all();
        $productsCount = 0;
        $productsSold = 0;
        foreach ($products as $product) {
            $productsCount += $product->available;
            $productsSold += $product->sold;
        }
        $stats['productsCount'] = $productsCount;
        $stats['productsSold'] = $productsSold;
        // End Products

        // Sales
        $sales = Sale::find()->all();
        $salesAmount = 0;
        foreach ($sales as $sale) {
            $salesAmount += $sale->cost;
        }
        $stats['salesCount'] = count($sales);
        $stats['salesAmount'] = $salesAmount;
        // End Sales

        // Customers
        $customersCount = count(Customer::find()->all());
        $stats['customersCount'] = $customersCount;
        // End Customers

        // Repair
        $stats['inRepairCount'] = count(Repair::find()->where(['status' => Repair::STATUS_INPROGRESS])->all());
        $stats['repairedCount'] = count(Repair::find()->where(['status' => Repair::STATUS_FIXED])->all());
        // End Repair

        return $this->render('index', [
            'stats' => $stats,
            'salesDisplay' => $salesDisplay,
            'salesSum' => $salesSum,
            'salesAmountDisplay' => $salesAmountDisplay,
            'salesAmountSum' => $salesAmountSum,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
