<?php
namespace backend\controllers;

use common\models\Category;
use common\models\Subcategory;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SettingsController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $categories = Category::find()->all();
        $newcategory = new Category();
        if ($newcategory->load(Yii::$app->request->post()) && $newcategory->save()) {
            Yii::$app->session->setFlash('success', 'Category added successfully!');
            return $this->redirect(['settings/index']);
        }
        $newuser = new User();
        if ($newuser->load(Yii::$app->request->post()) && $newuser->create()) {
            Yii::$app->session->setFlash('success', 'User added successfully!');
            return $this->redirect(['settings/index']);
        }
        $usersDataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'categories' => $categories,
            'newcategory' => $newcategory,
            'usersDataProvider' => $usersDataProvider,
            'newuser' => $newuser,
        ]);
    }

    public function actionCategory($id) {
        if (!$model = Category::findOne($id)) {
            Yii::$app->session->setFlash('error', 'Category can not be found!');
            return $this->redirect(['settings/index']);
        }
        if (Yii::$app->request->get('delete')) {
            try {
                if ($model->delete()) {
                    Yii::$app->session->setFlash('success', 'Category deleted successfully!');
                    return $this->redirect(['settings/index']);
                } else {
                    Yii::$app->session->setFlash('error', 'Category can not be deleted! Please delete its subcategories and related products first.');
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Category can not be deleted! Please delete its subcategories and related products first.');
            }
        }
        $newSub = new Subcategory();
        $newSub->category_id = $model->id;
        if ($newSub->load(Yii::$app->request->post()) && $newSub->save()) {
            $newSub = new Subcategory();
            Yii::$app->session->setFlash('success', 'Subcategory added successfully!');
        }
        $subs = Subcategory::find()->where('category_id = :id', [':id' => $model->id])->all();

        return $this->render('category', [
            'model' => $model,
            'newSub' => $newSub,
            'subs' => $subs,
        ]);
    }

    public function actionDeletesub($id) {
        if (!$model = Subcategory::findOne($id)) {
            Yii::$app->session->setFlash('error', 'Subcategory can not be found!');
            return $this->redirect(['settings/index']);
        }
        try {
            if ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Subcategory deleted successfully!');
            } else {
                Yii::$app->session->setFlash('error', 'Subcategory can not be deleted! Please delete its related products first.');
            }
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Category can not be deleted! Please delete its related products first.');
        }
        return $this->redirect(['settings/category', 'id' => $model->category_id]);
    }

    public function actionDeleteuser($id) {
        if (!$model = User::findOne($id)) {
            Yii::$app->session->setFlash('error', 'User can not be found!');
            return $this->redirect(['settings/index']);
        }
        if ($model->id == 1) {
            Yii::$app->session->setFlash('error', 'You can not delete the first admin user!');
            return $this->redirect(['settings/index']);
        }
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'User deleted!');
        }
        return $this->redirect(['settings/index']);
    }
}
