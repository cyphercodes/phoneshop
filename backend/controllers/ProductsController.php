<?php
namespace backend\controllers;

use common\models\Product;
use common\models\Subcategory;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class ProductsController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProduct($id = null) {
        $model = new Product();
        $model->available = 1;
        if ($id && $find = Product::findOne($id)) {
            $model = $find;
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->tagsTmp = Yii::$app->request->post()['Product']['tagsTmp'];
            if (($model->imgTmp = UploadedFile::getInstance($model, 'imgTmp'))) {
                $model->uploadImg();
                Yii::$app->session->setFlash('success', 'Image changed successfully!');
                return $this->redirect(['products/product/' . $model->id]);
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Product saved successfully');
                return $this->redirect(['products/product/' . $model->id]);
            }
        } else {
            $model->tagsTmp = $model->tagsarray;
        }
        return $this->render('_product', [
            'model' => $model,
        ]);
    }

    public function actionListSubcategories($query = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($query)) {
            $models = Subcategory::find()
                ->andWhere(['category_id' => $query])
                ->all();
            $items = [];
            foreach ($models as $model) {
                $items[] = ['id' => $model->id, 'text' => $model->title];
            }
            if (!empty($items)) {
                $out['results'] = $items;
            }
        }
        return $out;
    }


}
