<?php
namespace backend\controllers;

use common\models\Customer;
use common\models\Sale;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class CustomersController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Customer::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCustomer($id = null) {
        $model = new Customer();
        if ($id && $find = Customer::findOne($id)) {
            $model = $find;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Customer saved successfully');
            return $this->redirect(['customers/index']);
        }
        return $this->render('_customer', [
            'model' => $model,
        ]);
    }

    public function actionView($id) {
        $model = Customer::findOne($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Sale::find()->where(['customer_id' => $model->id]),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id) {
        if ($model = Customer::findOne($id)) {
            try {
                if ($model->delete()) {
                    Yii::$app->session->setFlash('success', 'Customer deleted successfully!');
                } else {
                    Yii::$app->session->setFlash('error', 'Customer can not be deleted! It has related past orders!');
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Customer can not be deleted! It has related past orders!');
            }
        }
        return $this->redirect(['customers/index']);
    }


}
