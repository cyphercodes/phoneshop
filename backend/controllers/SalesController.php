<?php
namespace backend\controllers;

use common\models\Customer;
use common\models\Product;
use common\models\Sale;
use common\models\Subcategory;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SalesController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Sale::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSale($id = null) {
        $model = new Sale();
        $model->quantity = 1;
        if ($id && $find = Sale::findOne($id)) {
            $model = $find;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Sale saved successfully');
            return $this->redirect(['sales/index']);
        }
        return $this->render('_sale', [
            'model' => $model,
        ]);
    }

    public function actionCalculateCost($productId, $quantity) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $arr = ['cost' => 0];
        if ($product = Product::findOne($productId)) {
            $arr['available'] = $product->realAvailable();
            $cost = $product->price * $quantity;
            $arr['cost'] = $cost;
        }
        return $arr;
    }

    public function actionDelete($id) {
        if ($model = Customer::findOne($id)) {
            try {
                if ($model->delete()) {
                    Yii::$app->session->setFlash('success', 'Customer deleted successfully!');
                } else {
                    Yii::$app->session->setFlash('error', 'Customer can not be deleted! It has related past orders!');
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Customer can not be deleted! It has related past orders!');
            }
        }
        return $this->redirect(['customers/index']);
    }


}
