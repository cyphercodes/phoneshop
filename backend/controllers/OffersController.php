<?php
namespace backend\controllers;

use common\models\Customer;
use common\models\Offer;
use common\models\Sale;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\Product;

/**
 * Site controller
 */
class OffersController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Offer::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOffer($id = null) {
        $model = new Offer();
        if ($id && $find = Offer::findOne($id)) {
            $model = $find;
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->productsTmp = Yii::$app->request->post()['Offer']['productsTmp'];
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Offer saved successfully');
                return $this->redirect(['offers/offer/' . $model->id]);
            }
        } else {
            $model->productsTmp = $model->productsarray;
//            var_dump($model->productsTmp);
//            die();
        }
        return $this->render('_offer', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        if ($model = Offer::findOne($id)) {
            if ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Offer deleted successfully');
            }
        }
        $this->redirect(['offers/index']);
    }

    public function actionView($id) {
        $model = Customer::findOne($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Sale::find()->where(['customer_id' => $model->id]),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCalculateCost() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cost = 0;
        $arr = [];
        if ($products = Product::findAll(Yii::$app->request->queryParams['productIds'])) {
            foreach ($products as $product) {
                $cost += $product->price;
            }
            $arr['cost'] = $cost;
        }
        return $arr;
    }


}
