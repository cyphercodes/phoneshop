<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Products';
?>
<h4 class="center teal-text">Products</h4>
<div class="card-panel white">
    <div class="card-content">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'employee-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                'title',
                [
                    'attribute' => 'price',
                    'value' => function ($data) {
                        return \common\components\Currency::format($data->price);
                    },
                ],
                ['attribute' => 'created_at',
                    'format' => ['date', 'php:' . \common\components\Currency::dateFormat()]
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => ['view' => function ($url, $model) {
                        return Html::a('<span class="material-icons">mode_edit</span>', ['products/product/' . $model->id]);
                    }],
                ],
            ],
        ]);
        ?>

        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add Product', ['products/product'], ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
    </div>
</div>