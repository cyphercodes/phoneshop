<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Products';
?>
    <h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'Edit'; ?> Product</h4>
    <div class="card-panel white">
        <div class="card-content">
            <?php
            $form = ActiveForm::begin([
                'id' => 'add-product-form',
                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <div class="center squareImgMaxHeight imgHover" id="menuimg">
                <label for="image-upload">
                    <img src="<?= $model->src() ?>"/>
                    <span class="text-content"><span>Change Picture</span></span>
                </label>
                <?php
                echo $form->field($model, 'imgTmp')->fileInput([
                    'class' => 'uploadimg',
                    'id' => 'image-upload',
                    'accept' => 'image/png, image/jpeg'])->label(false);
                ?>
            </div>
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'description') ?>
            <?= $form->field($model, 'price')->textInput(['placeholder' => 'In LBP']) ?>
            <?= $form->field($model, 'available')->textInput(['type' => 'number']); ?>
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'sold')->textInput(['type' => 'number', 'disabled' => 'disabled']);
            }
            ?>
            <?= $form->field($model, 'category_id')->dropDownList(\common\models\Category::listAll(), ['prompt' => 'Select Category...', 'class' => 'material']); ?>
            <?php
            if ($model->isNewRecord) {
                echo $form->field($model, 'subcategory_id')->dropDownList([], ['prompt' => 'Select Subcategory...', 'class' => 'material']);
            } else {
                echo $form->field($model, 'subcategory_id')->dropDownList(\common\models\Subcategory::listFor($model->category_id), ['prompt' => 'Select Subcategory...']);
            }
            ?>
            <?= $form->field($model, 'tagsTmp')->widget(\kartik\select2\Select2::classname(), [
                'data' => \common\models\Tag::allTags(),
//                'value' => $model->tagsTmp,
                'options' => ['placeholder' => 'Add tags (used for offer bundle matching)...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                ],
            ]);
            ?>

            <div class="center">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
                <?php // if (!$model->isNewRecord) { ?>
                <?php if (false) { ?>
                    <?= Html::a('Delete', Url::base(true) . '/products/product/' . $model->id . '?delete=delete', ['class' => 'waves-effect waves-light btn red']) ?>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJsFile("/js/category.js", ['depends' => 'yii\web\JqueryAsset', 'position' => \yii\web\View::POS_END], 'change-category');
?>