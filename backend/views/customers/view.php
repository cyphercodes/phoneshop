<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Customers';
?>
<div class="row">
    <div class="col l4 m12">
        <div class="card">
            <div class="card-content black-text">
                <span class="card-title"><?= $model->name; ?></span>
                <?php if ($model->email) { ?>
                    <p class="vam mv10"><span class="material-icons teal-text">email</span> <?= $model->email; ?>
                    </p>
                <?php } ?>
                <?php if ($model->phone) { ?>
                    <p class="vam mv10"><span class="material-icons teal-text">phone</span> <?= $model->phone; ?>
                    </p>
                <?php } ?>
            </div>
            <div class="card-action">
                <?= Html::a('Edit Customer', ['customers/customer', 'id' => $model->id]); ?>
                <!--                    <a href="#">Edit Customer</a>-->
            </div>
        </div>
    </div>
    <div class="col l4 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Purchases</span>
                <p class="mv10"><?= count($model->sales) ?></p>
            </div>
        </div>
    </div>
    <div class="col l4 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Total Spent</span>
                <p class="mv10"><?= \common\components\Currency::format($model->totalspent) ?></p>
            </div>
        </div>
    </div>
</div>
<?php if ($model->isDiscount()) { ?>
    <div class="card teal margin-bottom-20">
        <div class="card-content center white-text">
            This customer is eligible for discount!
        </div>
    </div>
<?php } ?>

<div class="card-panel white">
    <div class="card-content responsive-table">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'sales-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                [
                    'label' => 'Product',
                    'value' => function ($data) {
                        return $data->productname; // $data['name'] for array data, e.g. using SqlDataProvider.
                    },
                ],
                'quantity',
                [
                    'attribute' => 'cost',
                    'format' => 'Currency',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:' . \common\components\Currency::dateFormat()]
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => ['view' => function ($url, $model) {
                        return Html::a('<span class="material-icons">visibility</span>', ['sales/sale/' . $model->id]);
                    }],
                ],
            ],
        ]);
        ?>

    </div>
</div>