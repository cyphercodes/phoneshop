<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Customers';
?>
    <h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'Edit'; ?> Customer</h4>
    <div class="card-panel white">
        <div class="card-content">
            <?php
            $form = ActiveForm::begin([
                'id' => 'customer-form',
//                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'id')->textInput(['type' => 'number', 'disabled' => 'disabled']);
            }
            ?>
            <?= $form->field($model, 'firstname') ?>
            <?= $form->field($model, 'lastname') ?>
            <?= $form->field($model, 'phone') ?>
            <?= $form->field($model, 'email') ?>


            <div class="center">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
                <?php if (!$model->isNewRecord) { ?>
                    <?= Html::a('Delete', Url::base(true) . '/customers/delete/' . $model->id, ['class' => 'waves-effect waves-light btn red']) ?>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJsFile("/js/category.js", ['depends' => 'yii\web\JqueryAsset', 'position' => \yii\web\View::POS_END], 'change-category');
?>