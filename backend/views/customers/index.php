<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Customers';
?>
<h4 class="center teal-text">Customers</h4>
<div class="card-panel white">
    <div class="card-content responsive-table">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'customers-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                'firstname',
                'lastname',
                'phone',
                'email',
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{view}{edit}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="material-icons">visibility</span>', ['customers/view/' . $model->id]);
                        },
                        'edit' => function ($url, $model) {
                            return Html::a('<span class="material-icons">mode_edit</span>', ['customers/customer/' . $model->id]);
                        }
                    ],
                ],
            ],
        ]);
        ?>

        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add', ['customers/customer'], ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
    </div>
</div>