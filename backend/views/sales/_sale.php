<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Sales';
?>
    <h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'View'; ?> Sale</h4>
    <div class="card-panel white">
        <div class="card-content">
            <?php
            $form = ActiveForm::begin([
                'id' => 'sale-form',
            ]);
            ?>
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'id')->textInput(['type' => 'number', 'disabled' => 'disabled']);
                ?>
                <div class="form-group">
                    <label class="control-label active">Product</label>
                    <?php if ($model->product_id) { ?>
                        <a href="<?= Url::toRoute('products/product') . '/' . $model->product_id; ?>">
                            <input type="text" class="form-control" value="<?= $model->productname ?>" disabled="disabled">
                        </a>
                    <?php } else { ?>
                        <input type="text" class="form-control" value="<?= $model->productname ?>" disabled="disabled">
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label class="control-label active">Customer</label>
                    <?php if ($model->product_id) { ?>
                        <a href="<?= Url::toRoute('customers/customer') . '/' . $model->customer_id; ?>">
                            <input type="text" class="form-control" value="<?= $model->customername ?>" disabled="disabled">
                        </a>
                    <?php } else { ?>
                        <input type="text" class="form-control" value="<?= $model->customername ?>" disabled="disabled">
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label class="control-label active">Quantity</label>
                    <input type="text" class="form-control" value="<?= $model->quantity ?>" disabled="disabled">
                </div>
                <div class="form-group">
                    <label class="control-label active">Cost</label>
                    <input type="text" class="form-control" value="<?= \common\components\Currency::format($model->cost) ?>" disabled="disabled">
                </div>
            <?php } else { ?>
                <?= $form->field($model, 'product_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => \common\models\Product::listAvailable(),
                    'options' => ['placeholder' => 'Select a product ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
                <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => \common\models\Customer::listAll(),
                    'options' => ['placeholder' => 'Select a customer ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
                <?= $form->field($model, 'quantity')->textInput(['type' => 'number']); ?>
                <?= $form->field($model, 'cost'); ?>
            <?php } ?>
            <?= $form->field($model, 'note')->textarea(['class' => 'materialize-textarea']) ?>


            <div class="center">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
                <?php if (!$model->isNewRecord) { ?>
                    <?= Html::a('Delete', Url::base(true) . '/customers/delete/' . $model->id, ['class' => 'waves-effect waves-light btn red']) ?>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

<?php
$this->registerJsFile("/js/sale.js", ['depends' => 'yii\web\JqueryAsset', 'position' => \yii\web\View::POS_END], 'sale-js');
?>