<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Sales';
?>
<h4 class="center teal-text">Sales</h4>
<div class="card-panel white">
    <div class="card-content responsive-table">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'sales-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                [
                    'label' => 'Product',
                    'value' => function ($data) {
                        return $data->productname; // $data['name'] for array data, e.g. using SqlDataProvider.
                    },
                ],
                'quantity',
                [
                    'attribute' => 'cost',
                    'value'=> function($data){
                        return \common\components\Currency::format($data->cost);
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:' . \common\components\Currency::dateFormat()]
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => ['view' => function ($url, $model) {
                        return Html::a('<span class="material-icons">visibility</span>', ['sales/sale/' . $model->id]);
                    }],
                ],
            ],
        ]);
        ?>

        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add', ['sales/sale'], ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
    </div>
</div>