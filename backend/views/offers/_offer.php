<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Offer';
?>
    <h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'Edit'; ?> Offer</h4>
    <div class="card-panel white">
        <div class="card-content">
            <?php
            $form = ActiveForm::begin([
                'id' => 'add-offer-form',
//                'options' => ['enctype' => 'multipart/form-data']
            ]);
            ?>
            <?= $form->field($model, 'productsTmp')->widget(\kartik\select2\Select2::classname(), [
                'data' => \common\models\Product::allProductsArray(),
                'options' => ['placeholder' => 'Add Product', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                ],
            ]);
            ?>
            <?= $form->field($model, 'discount')->textInput(['type' => 'number', 'placeholder' => 'In Percentage', 'min' => 0, 'max' => 100]) ?>
            <?= $form->field($model, 'price')->textInput(['placeholder' => 'In LBP']) ?>

            <div class="center">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
                <?php if (!$model->isNewRecord) { ?>
                    <!--                --><?php //if (false) { ?>
                    <?= Html::a('Delete', Url::base(true) . '/offers/delete/' . $model->id, ['class' => 'waves-effect waves-light btn red']) ?>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJsFile("/js/offer.js", ['depends' => 'yii\web\JqueryAsset', 'position' => \yii\web\View::POS_END], 'change-category');
?>