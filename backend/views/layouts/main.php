<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\AppAsset;
use yii\helpers\Url;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
        var BASE_URL = '<?= Url::base(true); ?>';
    </script>
</head>
<body>
<?php $this->beginBody() ?>


<nav class="teal">
    <div class="nav-wrapper container">
        <a href="<?= Url::toRoute(['/site/index']); ?>" class="brand-logo">PhoneShop</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin())) { ?>
                <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isSales())) { ?>
                <li><a href="<?= Url::toRoute(['/products/index']); ?>">Products</a></li>
                <li><a href="<?= Url::toRoute(['/customers/index']); ?>">Customers</a></li>
                <li><a href="<?= Url::toRoute(['/sales/index']); ?>">Sales</a></li>
                <li><a href="<?= Url::toRoute(['/offers/index']); ?>">Offers</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isTechnical())) { ?>
                <li><a href="<?= Url::toRoute(['/technical/index']); ?>">Technical</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin())) { ?>
                <li><a href="<?= Url::toRoute(['/settings/index']); ?>">Settings</a></li>
            <?php } ?>
            <?php if (Yii::$app->user->isGuest) { ?>
                <li><a href="<?= Url::toRoute(['/site/login']); ?>">Login</a></li>
            <?php } else { ?>
                <li><a href="<?= Url::toRoute(['/site/logout']); ?>">Logout</a></li>
            <?php } ?>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin())) { ?>
                <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isSales())) { ?>
                <li><a href="<?= Url::toRoute(['/products/index']); ?>">Products</a></li>
                <li><a href="<?= Url::toRoute(['/customers/index']); ?>">Customers</a></li>
                <li><a href="<?= Url::toRoute(['/sales/index']); ?>">Sales</a></li>
                <li><a href="<?= Url::toRoute(['/offers/index']); ?>">Offers</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isTechnical())) { ?>
                <li><a href="<?= Url::toRoute(['/technical/index']); ?>">Technical</a></li>
            <?php } ?>
            <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin())) { ?>
                <li><a href="<?= Url::toRoute(['/settings/index']); ?>">Settings</a></li>
            <?php } ?>
            <?php if (Yii::$app->user->isGuest) { ?>
                <li><a href="<?= Url::toRoute(['/site/login']); ?>">Login</a></li>
            <?php } else { ?>
                <li><a href="<?= Url::toRoute(['/site/logout']); ?>">Logout</a></li>
            <?php } ?>
        </ul>
    </div>
</nav>
<main>
    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="page-footer teal">
    <div class="footer-copyright teal">
        <div class="container">
            © <?= date('Y') ?> PhoneShop
            <span class="grey-text text-lighten-4 right">Created by Ahmad Ginzarly</span>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
