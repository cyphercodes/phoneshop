<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Settings';
?>
<h4 class="center teal-text">Users</h4>
<div class="card-panel white">
    <div class="card-content responsive-table">
        <?php
        echo GridView::widget([
            'dataProvider' => $usersDataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'employee-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                'email',
                'typelabel',
                ['attribute' => 'created_at',
                    'format' => ['date', 'php:' . \common\components\Currency::dateFormat()]
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => ['delete' => function ($url, $model) {
                        return Html::a('<span class="material-icons">delete</span>', ['settings/deleteuser/' . $model->id]);
                    }],
                ],
            ],
        ]);
        ?>

        <?php
        $form = ActiveForm::begin([
            'id' => 'new-user',
        ]);
        ?>
        <div class="row">
            <div class="col m4 s12">
                <?= $form->field($newuser, 'email'); ?>
            </div>
            <div class="col m4 s12">
                <?= $form->field($newuser, 'password')->passwordInput(); ?>
            </div>
            <div class="col m4 s12">
                <?= $form->field($newuser, 'type')->dropDownList([0 => 'Admin', 10 => 'Sales', 20 => 'Technical'], ['class' => 'material']); ?>
            </div>
        </div>
        <div class="center">
            <?= Html::submitButton('<i class="material-icons left">add</i> Add User', ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<hr/>
<h4 class="center teal-text">Categories</h4>
<div class="card-panel white">
    <div class="card-content">
        <div class="collection">
            <?php if (count($categories) < 1) { ?>
                <a href="#" class="collection-item center">No categories available yet...</a>
            <?php } ?>
            <?php foreach ($categories as $category) { ?>
                <a href="<?= Url::toRoute(['settings/category']) . '/' . $category->id ?>" class="collection-item"><?= $category->title ?></a>
            <?php } ?>
        </div>

        <?php
        $form = ActiveForm::begin([
            'id' => 'new-category',
        ]);
        ?>
        <?= $form->field($newcategory, 'title'); ?>
        <div class="center">
            <?= Html::submitButton('<i class="material-icons left">add</i> Add Category', ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

