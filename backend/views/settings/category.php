<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Category';
?>

<h4 class="center teal-text"><?= $model->title; ?></h4>
<div class="center margin-bottom-20">
    <?= Html::a('Delete', Url::toRoute(['settings/category']) . '/' . $model->id . '?delete=1', ['class' => 'waves-effect btn red']); ?>
</div>
<div class="card-panel white">
    <div class="card-content">
        <ul class="collection with-header">
            <li class="collection-header"><h4>Subcategories</h4></li>
            <?php if (count($subs) < 1) { ?>
                <li class="collection-item center">No subcategories available yet...</li>
            <?php } ?>
            <?php foreach ($subs as $sub) { ?>
                <li class="collection-item"><?= $sub->title ?>
                    <a href="<?= Url::toRoute(['settings/deletesub']) . '/' . $sub->id; ?>" class="secondary-content"><i class="material-icons red-text">delete</i></span>
                    </a>
                </li>
            <?php } ?>
        </ul>

        <?php
        $form = ActiveForm::begin([
            'id' => 'new-category',
        ]);
        ?>
        <?= $form->field($newSub, 'title'); ?>
        <div class="center">
            <?= Html::submitButton('<i class="material-icons left">add</i> Add Subcategory', ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

