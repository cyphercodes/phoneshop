<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Technical';
?>
<h4 class="center teal-text">Technical</h4>
<div class="card-panel white">
    <div class="card-content responsive-table">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'highlight centered margin-bottom-20 responsive-table', 'id' => 'sales-list'],
            'responsive' => false,
            'responsiveWrap' => false,
            'columns' => [
                'id',
                'title',
                [
                    'label' => 'Status',
                    'value' => function ($data) {
                        return '<span class="' . $data->statusColor() . '-text">' . $data->statusString() . '</span>'; // $data['name'] for array data, e.g. using SqlDataProvider.
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:' . \common\components\Currency::dateFormat()]
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => ['view' => function ($url, $model) {
                        return Html::a('<span class="material-icons">visibility</span>', ['technical/repair/' . $model->id]);
                    }],
                ],
            ],
        ]);
        ?>

        <div class="center">
            <?= Html::a('<i class="material-icons left">add</i> Add', ['technical/repair'], ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
        </div>
    </div>
</div>