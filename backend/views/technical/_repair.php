<?php

/* @var $this yii\web\View */
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Sales';
?>
    <h4 class="center teal-text"><?= $model->isNewRecord ? 'Add' : 'View'; ?> Repair</h4>
    <div class="card-panel white">
        <div class="card-content">
            <?php
            $form = ActiveForm::begin([
                'id' => 'repair-form',
            ]);
            ?>
            <?php if (!$model->isNewRecord) {
                echo $form->field($model, 'id')->textInput(['type' => 'number', 'disabled' => 'disabled']);
            } ?>

            <?= $form->field($model, 'title'); ?>
            <?= $form->field($model, 'product_id'); ?>
            <?php if (!$model->isNewRecord) {
                echo $form->field($model, 'status')->dropDownList(\common\models\Repair::statusOptions(), ['class' => 'material']);
            } ?>

            <div class="center">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Edit', ['class' => 'waves-effect waves-light btn']) ?>
                <?php if (!$model->isNewRecord) { ?>
                    <?= Html::a('Delete', Url::base(true) . '/repair/delete/' . $model->id, ['class' => 'waves-effect waves-light btn red']) ?>
                <?php } ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

<?php
$this->registerJsFile("/js/sale.js", ['depends' => 'yii\web\JqueryAsset', 'position' => \yii\web\View::POS_END], 'sale-js');
?>