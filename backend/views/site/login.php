<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<h4 class="center teal-text">Login</h4>
<div class="card-panel white">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
    ]);
    ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe', ['template' => "{input} {label}",])->checkbox([], false) ?>


    <div class="center">
        <?= Html::submitButton('Login', ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
