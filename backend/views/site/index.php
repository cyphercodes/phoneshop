<?php

/* @var $this yii\web\View */
use kartik\daterange\DateRangePicker;
use miloschuman\highcharts\Highcharts;

$this->title = 'Home';
?>
<h4 class="center teal-text">Overview Report</h4>

<h5>By Date</h5>
<form action="" method="get">
    <div class="row">
        <div class="col m10">
            <?php
            echo DateRangePicker::widget([
                'name' => 'date_range',
                "value" => (isset(Yii::$app->request->get()['date_range']) && !empty(Yii::$app->request->get()['date_range'])) ? Yii::$app->request->get()['date_range'] : "",
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'DD-MM-YYYY',
                        'separator' => ' to ',
                    ]
                ]
            ]);
            ?>
        </div>
        <div class="col m2">
            <button type="submit" class="btn center"> Submit</button>
        </div>
    </div>
    <div class="row">
        <div class="col l6 m12">
            <div class="card stat">
                <div class="card-content black-text">
                    <span class="card-title">Sales Count</span>
                    <p class="mv10 center"><?= $salesSum ?></p>
                </div>
            </div>
        </div>
        <div class="col l6 m12">
            <div class="card stat">
                <div class="card-content black-text">
                    <span class="card-title">Sales Amount</span>
                    <p class="mv10 center"><?= \common\components\Currency::format($salesAmountSum) ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php if ($salesDisplay) { ?>
        <?php
        echo Highcharts::widget([
            'options' => [
                'title' => null,
                'chart' => [
                    'type' => 'spline',
                    'backgroundColor' => null,
                ],
                'xAxis' => [
                    'type' => 'datetime',
                ],
                'yAxis' => [
                    [
                        'labels' => [
                            'format' => '{value} Sales',
                        ],
                        'title' => [
                            'text' => 'Sales Count',
                        ],
                        'visible' => false,
                    ],
                    [// Secondary yAxis
                        'title' => [
                            'text' => 'Sales Amount',
                        ],
                        'labels' => [
                            'format' => '{value} L.L',
                        ],
                        'visible' => false,
                    ],
//                [// Secondary yAxis
//                    'title' => [
//                        'text' => 'Delivery Items',
//                    ],
//                    'labels' => [
//                        'format' => '{value} Items',
//                    ],
//                    'visible' => false,
//                ],
                ],
                'legend' => [
                    "layout" => "vertical",
                    "align" => "right",
                    "verticalAlign" => "middle",
                    "borderWidth" => 0,
                ],
                'series' => [
                    [
                        'name' => 'Sales Count',
                        'data' => $salesDisplay,
                        'yAxis' => 0,
                        'tooltip' => [
                            'valueSuffix' => ' sales'
                        ]
                    ],
                    [
                        'name' => 'Sales Amount',
                        'data' => $salesAmountDisplay,
                        'yAxis' => 1,
                        'tooltip' => [
                            'valueSuffix' => ' L.L',
                        ]
                    ],
//                [
//                    'name' => 'Items',
//                    'data' => $deliveryStatComponents['deliveryItems'],
//                    'yAxis' => 2,
//                    'tooltip' => [
//                        'valueSuffix' => ' items'
//                    ]
//                ]
                ],
                'tooltip' => [
                    'shared' => true,
                ],
                'credits' => ['enabled' => false],
            ]
        ]);
        ?>
        <hr/>
    <?php } ?>
</form>
<h5>Overall</h5>
<div class="row">
    <div class="col l4 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Products Sold</span>
                <p class="mv10"><?= $stats['productsSold'] ?></p>
            </div>
        </div>
    </div>
    <div class="col l4 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Products Unsold</span>
                <p class="mv10"><?= $stats['productsCount'] - $stats['productsSold'] ?></p>
            </div>
        </div>
    </div>
    <div class="col l4 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Total Products</span>
                <p class="mv10"><?= $stats['productsCount'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col l6 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Sales Count</span>
                <p class="mv10"><?= $stats['salesCount'] ?></p>
            </div>
        </div>
    </div>
    <div class="col l6 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Total Sales</span>
                <p class="mv10"><?= \common\components\Currency::format($stats['salesAmount']) ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col l6 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Items in Repair</span>
                <p class="mv10"><?= $stats['inRepairCount'] ?></p>
            </div>
        </div>
    </div>
    <div class="col l6 m12">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Items Repaired</span>
                <p class="mv10"><?= $stats['repairedCount'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col l6 m12 offset-l3">
        <div class="card stat">
            <div class="card-content black-text">
                <span class="card-title">Customers</span>
                <p class="mv10"><?= $stats['customersCount'] ?></p>
            </div>
        </div>
    </div>
</div>