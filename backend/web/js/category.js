$(document).on('change', '#product-category_id', function () {
    parseSubs($(this).val())
});
$(document).ready(function () {
    var categoryDropdown = $('#product-category_id');
    if (isInt(parseInt(categoryDropdown.val()))) {
        parseSubs(categoryDropdown.val());
    }
});

var parseSubs = function (val) {
    var selectDropdown = $("#product-subcategory_id");
    if (isInt(parseInt(selectDropdown.val()))) {
        var savedVal = selectDropdown.val();
    }
    selectDropdown.empty().html(' ');

    $.get("/products/list-subcategories", {query: val}).done(function (data) {
        for (var i = 0; i < data.results.length; i++) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", data.results[i].id)
                    .text(data.results[i].text)
            );
        }
        if (data.results.length < 1) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", null)
                    .text("Please add subcategories under this category to be able to use it...")
            );
        }
        selectDropdown.val(savedVal);
        selectDropdown.material_select();
    });

};

var isInt = function (n) {
    return Number(n) === n && n % 1 === 0;
};

$(document).ready(function () {

    var form = $('#add-product-form');

    var currImage = {};
    var currSelection = {};

    // prepare instant preview
    $(".uploadimg").on("change", function () {
        var inputField = $(this);

        // prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(inputField.get(0).files[0]);

        oFReader.onload = function (oFREvent) {
            var img = new Image();
            img.src = oFReader.result;
            img.onload = function () {
                if (this.width < 600 || this.height < 600) {
                    alert("Image should be at least 600 x 600!");
                    clearFileInputs();
                    return;
                }
                form.yiiActiveForm('submitForm');
            }
        };

    });


    var clearFileInputs = function () {
        $('.uploadimg').each(function (i, obj) {
            $(this).replaceWith($(this).val("").clone(true));
        });
    }

});