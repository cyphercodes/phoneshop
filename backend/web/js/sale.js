$(document).on('change', '#sale-product_id', function () {
    calculateCost();
});
$(document).on('change', '#sale-quantity', function () {
    calculateCost();
});
$(document).on('change', '#sale-customer_id', function () {
    checkDiscount();
});
$(document).ready(function () {
    var categoryDropdown = $('#product-category_id');
    if (isInt(parseInt(categoryDropdown.val()))) {
        parseSubs(categoryDropdown.val());
    }
});

var calculateCost = function () {
    var productInput = $('#sale-product_id');
    var quantityInput = $('#sale-quantity');
    var costInput = $('#sale-cost');

    var productId = productInput.val();
    var quantity = quantityInput.val();

    if (quantity < 1) {
        quantityInput.val(1).change();
    }

    $.get("/sales/calculate-cost", {productId: productId, quantity: quantity}).done(function (data) {
        if (typeof (data.available) != 'undefined') {
            $('.field-sale-quantity label').text('Quantity (' + data.available + ' available)')
            if (quantity > data.available) {
                quantityInput.val(data.available).change();
            }
        } else {
            $('.field-sale-quantity label').text('Quantity');
        }

        costInput.val(data.cost);

    });

};
var parseSubs = function (val) {
    var selectDropdown = $("#product-subcategory_id");
    if (isInt(parseInt(selectDropdown.val()))) {
        var savedVal = selectDropdown.val();
    }
    selectDropdown.empty().html(' ');

    $.get("/products/list-subcategories", {query: val}).done(function (data) {
        for (var i = 0; i < data.results.length; i++) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", data.results[i].id)
                    .text(data.results[i].text)
            );
        }
        if (data.results.length < 1) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", null)
                    .text("Please add subcategories under this category to be able to use it...")
            );
        }
        selectDropdown.val(savedVal);
        selectDropdown.material_select();
    });

};

var isInt = function (n) {
    return Number(n) === n && n % 1 === 0;
};

var checkDiscount = function () {
    var customerInput = $('#sale-customer_id');

    var customerId = customerInput.val();

    if (customerId) {
        $.get("/sales/check-discount", {id: customerId}).done(function (data) {
            if ((typeof (data.eligible) != 'undefined') && data.eligible) {
                Materialize.toast("This customer is eligible for discount!", 5000, 'success');
            } else {

            }

        });
    } else {

    }

};

$(document).ready(function () {

    var form = $('#add-product-form');

    var currImage = {};
    var currSelection = {};

    // prepare instant preview
    $(".uploadimg").on("change", function () {
        var inputField = $(this);

        // prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(inputField.get(0).files[0]);

        oFReader.onload = function (oFREvent) {
            var img = new Image();
            img.src = oFReader.result;
            img.onload = function () {
                if (this.width < 600 || this.height < 600) {
                    alert("Image should be at least 600 x 600!");
                    clearFileInputs();
                    return;
                }
                form.yiiActiveForm('submitForm');
            }
        };

    });


    var clearFileInputs = function () {
        $('.uploadimg').each(function (i, obj) {
            $(this).replaceWith($(this).val("").clone(true));
        });
    }

});