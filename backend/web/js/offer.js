$(document).on('change', '#sale-product_id', function () {
    calculateCost();
});
$(document).on('change', '#offer-productstmp, #offer-discount', function () {
    calculateCost();
});

var calculateCost = function () {
    var productInput = $('#offer-productstmp');
    var discountInput = $('#offer-discount');
    var costInput = $('#offer-price');

    var productIds = productInput.val();
    var discount = discountInput.val();

    $.get("/offers/calculate-cost", {productIds: productIds}).done(function (data) {
        var cost = isNaN(costInput) ? 0 : costInput;
        if ((typeof(data.cost) != 'undefined') && (data.cost > 0)) {
            cost = data.cost;
        }
        if (discount != '') {
            cost = Math.round(cost - (cost * (discount / 100)), 1);
        }
        costInput.val(cost);
    });

};
var parseSubs = function (val) {
    var selectDropdown = $("#product-subcategory_id");
    if (isInt(parseInt(selectDropdown.val()))) {
        var savedVal = selectDropdown.val();
    }
    selectDropdown.empty().html(' ');

    $.get("/products/list-subcategories", {query: val}).done(function (data) {
        for (var i = 0; i < data.results.length; i++) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", data.results[i].id)
                    .text(data.results[i].text)
            );
        }
        if (data.results.length < 1) {
            selectDropdown.append(
                $("<option></option>")
                    .attr("value", null)
                    .text("Please add subcategories under this category to be able to use it...")
            );
        }
        selectDropdown.val(savedVal);
        selectDropdown.material_select();
    });

};

var isInt = function (n) {
    return Number(n) === n && n % 1 === 0;
};
